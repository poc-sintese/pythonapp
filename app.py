import os
from flask import Flask
from redis import Redis

app = Flask(__name__)
redisDb = Redis(host=os.getenv('REDIS_HOST'), port=os.getenv('REDIS_PORT'))

@app.route('/')
def welcome():
    redisDb.incr('visitorCount')
    visitCount = str(redisDb.get('visitorCount'), 'utf-8')
    return "Welcome to visitor count: " + visitCount

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)

